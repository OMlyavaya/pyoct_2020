import pytest


class User:
    def __init__(self, name, age):
        self.name = name
        self.age = age

@pytest.fixture
def get_user2():
    return User(name="Vasya", age="40")

@pytest.fixture
def get_user1():
    return User(name="Vasya", age="40")